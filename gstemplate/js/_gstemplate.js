/* Implement custom javascript here */
 /*
 * Alexandru Dragu
 * 2017 
 */
/* Hide show Top Bar */
(function ($){
	$(document).ready(function() {
                var previousScroll = 0;
            $(window).scroll(function(event){
               var scroll = $(this).scrollTop();
               if (scroll > previousScroll){
                   $(".topheader").filter(':not(:animated)').slideUp();
               } else {
                  $(".topheader").filter(':not(:animated)').slideDown();
               }
               previousScroll = scroll;
            });
        });
})(jQuery);

// Add Value to Appointment Input
(function($){
$(document).ready(function() {
$( "#edit-submitted-appointment-date" ).val( 'Appointment Date' );
});
})(jQuery);

// Data-equalizer
(function($){
     $(document).foundation({
    equalizer: {
        equalize_on_stack: true
    }
}); })(jQuery);

(function($){
 $(document).ready(function(){
  $('#panel20').addClass('current');
  $('ul.tabs li').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  });

});
})(jQuery);

(function($){
 $(document).ready(function () {
    console.log('Js App loaded successfully, written by Alexandru Dragu');
     });
})(jQuery);